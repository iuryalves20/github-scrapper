# coding: utf-8

import os
import sys
import multiprocessing
import time
from pprint import pprint
from datetime import datetime
from utils import read_lines
from repository import Repository
from scraper import GitHubScraper

'''
    File name: main.py
    Version: 0.1
    Python Version: 3.6
    author: ...
    email:  ...
'''

__version__ = '0.1'

DEBUG = False
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DOMAIN = 'https://github.com'
FILE_NAME = 'repositories.txt'


def run_scrap(file_info):
    '''
    :param project_url: URL do projeto que sera lido
    :type project_url: str
    :param _domain:
    :type _domain: str
    :param _scraper:
    :param _scraper: crawler.GitHubScraper
    :return: dict
    '''

    if DEBUG:
        print('[INFO.{dt:%Y/%m/%d %H:%M:%S}][PID.{pid}] FILE {_info}'.format(
            dt=datetime.now(), pid=os.getpid(), _info=file_info)
        )

    return file_info.extension, file_info.get_bytes(), file_info.get_lines()


if __name__ == "__main__":
    print('[INFO.{dt:%Y/%m/%d %H:%M:%S}][PID.{pid}] START'.format(dt=datetime.now(), pid=os.getpid()))

    # Concatena o diretorio do projeto com o nome do arquivo que sera lido, obtendo o caminho completo
    _file_path = os.path.join(BASE_DIR, FILE_NAME)
    before = time.clock()
    # Instancia do objeto Pool para abrir processos com a função Map
    # O número máximo de processo abertos será o numero de núcleos do processador
    print('[INFO.{dt:%Y/%m/%d %H:%M:%S}][PID.{pid}] CPU_COUNT {cpu}'.format(dt=datetime.now(), pid=os.getpid(),
                                                                            cpu=multiprocessing.cpu_count()))
    pool = multiprocessing.Pool(processes=multiprocessing.cpu_count())
    results = {}
    # Função: run_scrap()
    # read_lines(_file_path) vai retornar uma lista que cada elemento será passado como o parâmetro project_url

    for line in read_lines(_file_path):
        repository = Repository(url=line, domain=DOMAIN, scraper=GitHubScraper)
        for result in pool.map(run_scrap, repository.list_files()):
            extension, bytes_, lines = result
            if extension not in results:
                results[extension] = {
                    'bytes': 0.0,
                    'lines': 0
                }
            results[extension]['bytes'] += bytes_
            results[extension]['lines'] += lines


    # print('[INFO.{dt:%Y/%m/%d %H:%M:%S}][PID.{pid}] RESULTS {_result}'.format(dt=datetime.now(), pid=os.getpid(),
    #                                                                           _result=results))
    pprint(results)
    elapsed = time.clock() - before
    print('[INFO.Time Elapsed: {elapsed}][PID.{pid}] END'.format(elapsed=elapsed, pid=os.getpid()))
    sys.exit(0)
