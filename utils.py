# coding: utf-8


def read_lines(file_path):
    with open(file_path) as file_:
        return file_.read().splitlines()
