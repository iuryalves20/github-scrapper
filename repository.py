# coding: utf-8

from pprint import pprint
from models import File


class Repository(object):

    def __init__(self, url, domain=None, scraper=None):
        if not url.startswith('/'):
            url = '/' + url
        self.url = url
        self.domain = domain
        self.files = []
        self.scraper = scraper()

    def list(self):
        for file in self.list_files():
            self.files.append(file)
        return self.files

    def tree(self):
        # Ordena os arquivos pela profundidade
        return sorted(self.files, key=lambda file_: file_.get_depth())

    def print_tree(self):
        pprint(self.tree())

    def list_files(self, url=None, parent=None):
        if url is None:
            url = self.url

        for link in self.scraper.get_links(self.domain + url):
            file_ = File(self.domain + link, parent, scraper=self.scraper)
            if file_.is_folder():
                yield from self.list_files(link, parent=file_)
            else:
                yield file_
