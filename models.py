# coding: utf-8
import re


class File(object):

    def __init__(self, url, parent=None, scraper=None):
        self.url = url
        self.parent = parent
        self.scraper = scraper
        self.name = url.split('/')[-1]
        self.extension = self.url.split('.')[-1]
        self._raw_info = None

    def __str__(self):
        return '{class_}(name={name},parent={parent})'.format(
            class_=self.__class__.__name__,
            name=self.name,
            parent=self.get_path()
        )

    __repr__ = __str__

    def get_depth(self):
        return self.get_path().count('/')

    def get_bytes(self):
        info = self.raw_info()
        return float(re.search(r'(\d+\.*\d*)', info).group(1).strip())

    def get_lines(self):
        info = self.raw_info()
        lines = re.search(r'(.+)lines', info)
        return int(lines.group(1).strip()) if lines else 0

    def get_path(self):
        parent = self.parent
        parents = []
        while parent:
            parents.append(parent.name)
            parent = parent.parent
        if not parents:
            return ''
        return '/' + '/'.join(reversed(parents))

    def is_folder(self):
        return '/tree/' in self.url

    def read(self):
        return self.scraper.get_content(self.url)

    def raw_info(self):
        if self._raw_info is None:
            self._raw_info = self.scraper.get_file_info(self.url)
        return self._raw_info
