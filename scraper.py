# coding: utf-8

import requests
from bs4 import BeautifulSoup


class GitHubScraper(object):

    def get_content(self, url):
        return requests.get(url).content

    def get_links(self, url):
        content = self.get_content(url)
        html_tree = BeautifulSoup(content, 'html.parser')
        for element in html_tree.find_all(self.filter_files):
            yield element.attrs['href']

    def get_file_info(self, url):
        content = self.get_content(url)
        html_tree = BeautifulSoup(content, 'html.parser')
        return html_tree.find(class_='file-info').text

    def filter_files(self, tag):
        return (
            tag.name == 'a' and
            tag.attrs.get('class', '') == ['js-navigation-open']
            and not tag.attrs.get('rel')
        )
