# coding: utf-8

"""
pip install requests beautifulsoup4
"""

import time
from pprint import pprint
from repository import Repository
from scraper import GitHubScraper
from utils import read_lines

DOMAIN = 'https://github.com'


def main():
    for project_url in read_lines('repositories.txt'):
        repository = Repository(project_url, domain=DOMAIN, scraper=GitHubScraper)
        result = {}
#        repository.list()
#        repository.print_tree()
        for file_info in repository.list_files():
            if file_info.extension not in result:
                result[file_info.extension] = {
                    'bytes': 0.0,
                    'lines': 0
                }

            result[file_info.extension]['bytes'] += file_info.get_bytes()
            result[file_info.extension]['lines'] += file_info.get_lines()
        pprint(result)


if __name__ == '__main__':
    before = time.clock()
    main()
    elapsed = time.clock() - before
    print('Time Elapsed {time}'.format(time=elapsed))
